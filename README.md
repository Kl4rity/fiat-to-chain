# Fiat to Chain

The purpose of this project is to create a proof of concept for a service
which will accept signed commands to be executed against the Ethereum Blockchain.

We're trying to take the burden of actually having to own crypto away from the
user and allow him to pay with fiat currencies instead. This should allow us to
keep most, if not all, of the guarantees of the Blockchain ecosystem  for the end user whilst significantly lowering
the barrier to entry.

## Local Setup
### Local Docker Compose Setup

To start the application you run `docker compose up -d` and then cd into the ./signing-server directory
to `./signing-server/gradlew clean run` to start the signing-server application.

The docker compose command not only contains all necessary dependencies for the Micronaut application but also
starts a local ganache-cli in-memory Ethereum chain and is then initialised with the contracts found in the ./contracts directory.