package com.cstift.config.bean.graphql

import graphql.GraphQL
import graphql.kickstart.tools.GraphQLResolver
import graphql.kickstart.tools.SchemaParser
import io.micronaut.context.annotation.Bean
import io.micronaut.context.annotation.Factory
import jakarta.inject.Singleton

@Factory
class GraphQLFactory {
    @Bean
    @Singleton
    fun graphQL(resolvers: List<GraphQLResolver<*>>): GraphQL {
        return GraphQL.newGraphQL(
            SchemaParser
                .newParser()
                .file("schema.graphqls")
                .resolvers(resolvers)
                .build()
                .makeExecutableSchema()
        ).build();
    }
}