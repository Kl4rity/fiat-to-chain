package com.cstift.config.bean.graphql

import com.cstift.transactions.mock.mockTransaction
import com.cstift.transactions.Transaction
import com.cstift.transactions.TransactionInput
import graphql.kickstart.tools.GraphQLMutationResolver
import jakarta.inject.Singleton

@Singleton
class Mutation : GraphQLMutationResolver {
    fun submitTransaction(input: TransactionInput): Transaction {
        return mockTransaction().copy(amount = input.amount);
    }
}