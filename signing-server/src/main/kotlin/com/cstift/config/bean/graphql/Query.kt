package com.cstift.config.bean.graphql

import com.cstift.transactions.mock.mockTransaction
import com.cstift.transactions.Transaction
import graphql.kickstart.tools.GraphQLQueryResolver
import jakarta.inject.Singleton

@Singleton
internal class Query : GraphQLQueryResolver {
    fun transactions(): List<Transaction> {
        return arrayListOf (
            mockTransaction(),
            mockTransaction()
        );
    }
}