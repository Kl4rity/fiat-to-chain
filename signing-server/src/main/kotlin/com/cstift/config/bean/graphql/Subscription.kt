package com.cstift.config.bean.graphql

import com.cstift.transactions.mock.mockTransaction
import com.cstift.transactions.Transaction
import graphql.kickstart.tools.GraphQLSubscriptionResolver
import jakarta.inject.Singleton
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.reactive.asPublisher
import org.reactivestreams.Publisher

@Singleton
class Subscription: GraphQLSubscriptionResolver {
    fun transactions(): Publisher<Transaction> {
        return flow {
            while (true) {
                // TODO: Not production ready this way. Just Tech Spike.
                this.emit(mockTransaction());
                kotlinx.coroutines.delay(5000)
            }
        }.asPublisher()
    }
}