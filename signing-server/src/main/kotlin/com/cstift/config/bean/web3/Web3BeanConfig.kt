package com.cstift.config.bean.web3

import io.micronaut.context.annotation.Bean
import io.micronaut.context.annotation.Factory
import jakarta.inject.Singleton
import org.web3j.minimalforwarder.MinimalForwarder
import org.web3j.tx.gas.DefaultGasProvider

@Factory
class Web3BeanConfig {
    @Bean
    @Singleton
    fun minimalForwarder(forwarderConfig: MinimalForwarderConfig): MinimalForwarder {
        return MinimalForwarder.load(
            contractAddress = forwarderConfig.address,
            web3j = ,
            transactionManager = ,
            contractGasProvider = DefaultGasProvider())
    }
}