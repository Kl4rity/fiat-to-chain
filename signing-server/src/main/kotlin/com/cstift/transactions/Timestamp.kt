package com.cstift.transactions

import java.time.LocalDateTime

data class Timestamp(
    val epoch: Number,
    val iso8601: LocalDateTime,
)