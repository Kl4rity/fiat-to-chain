package com.cstift.transactions

import java.math.BigDecimal
import java.util.*

data class Transaction(
    val id: UUID,
    val timestamp: Timestamp,
    val amount: BigDecimal,
    val txId: String,
)