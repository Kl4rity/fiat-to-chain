package com.cstift.transactions

import java.math.BigDecimal

data class TransactionInput(
    val amount: BigDecimal
)