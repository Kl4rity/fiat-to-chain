package com.cstift.transactions.mock

import com.cstift.transactions.Timestamp
import com.cstift.transactions.Transaction
import java.math.BigDecimal
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.util.*


fun mockTransaction(): Transaction {
    return Transaction(
        id = UUID.randomUUID(),
        amount = BigDecimal(200),
        timestamp = Timestamp(
            LocalDateTime.now().toEpochSecond(ZoneOffset.UTC),
            LocalDateTime.now()
        ),
        txId = UUID.randomUUID().toString()
    )
}